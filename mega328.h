#ifndef MEGA328_H_INCLUDED
#define MEGA328_H_INCLUDED

/// Pin Defines
#define LCD_DREG        DDRD
#define LCD_DPORT       PORTD
#define LCD_DPIN        PIND

#define LCD_CREG        DDRB
#define LCD_CPORT       PORTB
#define LCD_reset       0       // Not controlled
#define LCD_rs          PB0
#define LCD_rw          PB1
#define LCD_e           PB3

/*
#define LCD_db0         PD0
#define LCD_db1         PD1
#define LCD_db2         PD2
#define LCD_db3         PD3
#define LCD_db4         PD4
#define LCD_db5         PD5
#define LCD_db6         PD6
#define LCD_db7         PD7
*/

#endif // MEGA328_H_INCLUDED
