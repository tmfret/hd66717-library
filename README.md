# README #

### What is this repository for? ###

* HD66717 LCD Driver Library
* Version: Beta

### How do I get set up? ###

* I use Codeblocks for mcu programming. There is no makefile associated with this library. Please open the .cbp project file and make the example programs from there
* To install, just copy to your project's library directory, include the proper pin definition header, and include the HD66717 library header
* I have written some pin definition files for micro controllers I use often. If you make a pin definition file for another micro, it can be added to the repository
* My particular LCD is also hard-wired for 8-bit mode. I will not be implementing 4-bit mode unless I buy an LCD with more flexibility

### Contribution guidelines ###

* I am totally open to suggestions to speed this library up, or add additional functionality. I crerated this to interface with a 24x1 LCD, and have not tested it with any other display size. However, it should be easy enough to modify the LCD_init() function to allow different configurations

### Who do I talk to? ###

* Tyler McLaughlin <tmfret@gmail.com>