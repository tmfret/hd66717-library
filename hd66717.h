#ifndef HD66717_H_INCLUDED
#define HD66717_H_INCLUDED

#include <avr/io.h>

#include "mega328.h"

/*
To set up the display:
RS   LOW  (LOW for command, HIGH for data)
0x1C   Turn on LCD driver power
0x14   Turn on character display
0x28   set both 12 character lines to 24 X 1
0x4F   set contrast to darkest (3F to 4F is best range to choose from)
0xE0   set data address to first character (Home cursor)

To clock a byte
E    line low
BYTE
E    line high
*/

/*
RS  R/W     Operation
0   0       IR write as an internal operation (display clear, etc.)
0   1       Read busy flag (DB7) read and address counter (DB0 to DB6) (4/8-bit bus interface)
1   0       DR write as an internal operation (DR to DDRAM, CGRAM, SEGRAM, or annunciator)
1   1       DR read as an internal operation (DDRAM, CGRAM, or SEGRAM to DR)
(4/8-bit bus interface)
*/

/// Register Defines
#define SR_MSK          0xFF            // Status R/W-1, RS-0
    #define SRAC0             0             // Address Counter 0
    #define SRAC1             1             // Address Counter 1
    #define SRAC2             2             // Address Counter 2
    #define SRAC3             3             // Address Counter 3
    #define SRAC4             4             // Address Counter 4
    #define SRAC5             5             // Address Counter 5
    #define SRAC6             6             // Address Counter 6
    #define SRBF              7             // Internally Operating 1- Busy

#define CL_MSK          0x01            // Clear Screen R/W-0, RS-0
#define CH_MSK          0x02            // Return Home R/W-0, RS-0
#define OS_MSK          0x03            // Start Oscillator R/W-0, RS-0
#define EM_MSK          0x07            // Entry Mode Set R/W-0, RS-0
    #define EMOSC             0             // System Clock 1- Divided By 4
    #define EMID              1             // Address Update Direction 0- Decrement, 1- Increment

#define CR_MSK          0x0F            // Cursor Control R/W-0, RS-0
    #define CRB             0               // Blink Cursor 1- On
    #define CRC             1               // 8th Raster-Row Cursor On 1- On
    #define CRBW            2               // Black-White Inverting Cursor 1- Inverting

#define DO_MSK          0x17            // Display On/Off Control R/W-0, RS-0
    #define DOLC            0               // Line Containing AC Given Cursor Attr
    #define DODS            1               // Segment Display 1- On
    #define DODC            2               // Character Display 1- On

#define PW_MSK          0x1F            // Power Control R/W-0, RS-0
    #define PWSTB           0               // Standby Mode 1- On
    #define PWSLP           1               // Sleep mode 1- On
    #define PWAMP           2               // Voltage Follower Booster 1- On

#define DC_MSK          0x3F            // Display Control R/W-0, RS-0
    #define DCDL1           0               // Double-Height Lines 1- First Line
    #define DCDL2           1               // Double-Height Lines 1- Second Line
    #define DCDL3           2               // Double-Height Lines 1- Third Line
    #define DCNL0           3               // Number of Display Lines
    #define DCNL1           4               // Number of Display Lines
                                                // 00- 1 line, 01- 2 Lines, 10- 3 Lines, 11- 4 Lines
#define CN_MSK          0x5F            // Contrast Control R/W-0, RS-0
    #define CNCT0           0               // Adjustment 0
    #define CNCT1           1               // Adjustment 1
    #define CNCT2           2               // Adjustment 2
    #define CNCT3           3               // Adjustment 3
    #define CNSN2           4               // Display Start Line 2

#define SC_MSK          0x7F            // Scroll Control R/W-0, RS-0
    #define SCSL0           0               // Display Start Raster-Row 0
    #define SCSL1           1               // Display Start Raster-Row 1
    #define SCSL2           2               // Display Start Raster-Row 2
                                                // 000- First Row, 111- Eighth Row
    #define SCSN0           3               // Display Start Line 0
    #define SCSN1           4               // Display Start Line 1
                                                // 000- First Line, 001- Second Line, 010- Third Line, 011- Fourth Line
                                                // 100- Fifth Line,

#define AS_MSK          0x9F            // Annunciator/SEGRAM Address Set R/W-0, RS-0
    #define ASAAN0          0               // Annunciator Address 0
    #define ASAAN1          1               // Annunciator Address 1
    #define ASAAN2          2               // Annunciator Address 2
    #define ASAAN3          3               // Annunciator Address 3
    #define ASDA            4               // Annunciator Display 1- On

#define CA_MSK          BF              // CGRAM Address Set R/W-0, RS-0
    #define CAA0            0               // Annunciator Address 0
    #define CAA1            1               // Annunciator Address 1
    #define CAA2            2               // Annunciator Address 2
    #define CAA3            3               // Annunciator Address 3
    #define CAA4            4               // Annunciator Display 1- On


#define DAH_MSK         0xC3            // DDRAM Address Set (Upper Bits) R/W-0, RS-0
    #define DAHA0           0               // DDRAM Address 0
    #define DAHA1           1               // DDRAM Address 1

#define DAL_MSK         0xFF            // DDRAM Address Set (Lower Bits) R/W-0, RS-0
    #define DALA0           0               // DDRAM Address 0
    #define DALA1           1               // DDRAM Address 1
    #define DALA2           2               // DDRAM Address 2
    #define DALA3           3               // DDRAM Address 3
    #define DALA4           4               // DDRAM Address 4

#define WD_MSK          0xFF            // Write Data to RAM R/W-0, RS-1
#define RD_MSK          0xFF            // Read Data From RAM R/W-1, RS-1

/// Constants
#define LCD_length      24 // Chars
#define LCD_height      1 // Lines
#define LCD_4bit        0 // Operate in 8 bit mode

#define LCD_CLRRS       (LCD_CPORT &= ~(1 << LCD_rs))
#define LCD_CLRRW       (LCD_CPORT &= ~(1 << LCD_rw))
#define LCD_SETRS       (LCD_CPORT |= (1 << LCD_rs))
#define LCD_SETRW       (LCD_CPORT |= (1 << LCD_rw))

#define LCD_CLRE        (LCD_CPORT &= ~(1 << LCD_e))
#define LCD_SETE        (LCD_CPORT |= (1 << LCD_e))

void LCD_init( void ); // Initialize LCD
void LCD_putc( uint8_t data ); // Send a byte to the lcd
void LCD_puts( uint8_t *str ); // Send a string to the lcd
void LCD_putCommand( uint8_t command ); // Send the lcd a command
uint8_t LCD_getc( void ); // Get the status byte and address count back from the LCD
void LCD_clear( void ); // Clear the LCD
void LCD_return ( void ); // Return to position 1
void LCD_wait( void ); // Wait for status bit

#endif // HD66717_H_INCLUDED
