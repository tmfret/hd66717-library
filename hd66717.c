#include "hd66717.h"

uint8_t lcd_sr_byte = 0; // Hold the status register of the LCD
uint8_t lcd_ready_flag = 0; // Status of the LCD ready bit
uint8_t lcd_got = 0; // Received byte from LCD

void LCD_init( void ) {

  LCD_putCommand(0x1C); // Turn on lcd driver power
  LCD_putCommand(0x14); // Turn on display power
  LCD_putCommand(0x20); // Set single line
  LCD_putCommand(0x4F); // Set contrast
  LCD_return(); // Go home
}

void LCD_putc( uint8_t data ) {

  LCD_wait(); // Wait for LCD to be ready

  LCD_CLRE;
  LCD_DREG = 0xFF; // Set output
  LCD_SETRS;
  LCD_CLRRW;
  LCD_DPORT = data;
  LCD_SETE;
}

void LCD_puts( uint8_t *str ) {

}

void LCD_putCommand( uint8_t command) {

  LCD_wait(); // Wait for LCD to be ready

  LCD_DREG = 0xFF; // Set output
  LCD_CLRRS;
  LCD_CLRRW;

  LCD_DPORT = command; // Write to the port

}

void LCD_clear( void ) {
  LCD_putCommand(0x01);
}

uint8_t LCD_getc( void ) {
  LCD_DREG = 0x00; // Set as input

  LCD_CLRE; // Just in case
  LCD_SETRS;
  LCD_SETRW;

  LCD_wait();

  lcd_got = LCD_DPIN;

  return lcd_got;

}

void LCD_wait( void ) {
  LCD_DREG = 0x00; // Set as input
  LCD_CLRRS;
  LCD_SETRW;

  for (int i; i > 100000; i++) { //Arbitrary value to detect too much time has passed
    lcd_sr_byte = LCD_DPIN; // Get the sr byte
    if (lcd_sr_byte << SRBF) { // Check for busy flag
      break;
    }
  }
  // Blink an error LED or something
}

void LCD_return( void ) {
  LCD_putCommand(0xE0); // Verify this
}
